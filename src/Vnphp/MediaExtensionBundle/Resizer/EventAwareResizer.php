<?php

namespace Vnphp\MediaExtensionBundle\Resizer;

use Gaufrette\File;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Resizer\ResizerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Vnphp\MediaExtensionBundle\EventListener\Resizer\GetBoxEvent;
use Vnphp\MediaExtensionBundle\EventListener\Resizer\ResizeEvent;
use Vnphp\MediaExtensionBundle\EventListener\Resizer\ResizerEvents;

class EventAwareResizer implements ResizerInterface
{
    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @var ResizerInterface
     */
    protected $delegate;

    /**
     * EventAwareResizer constructor.
     * @param EventDispatcherInterface $eventDispatcher
     * @param ResizerInterface $delegate
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, ResizerInterface $delegate)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->delegate = $delegate;
    }

    /**
     * @param MediaInterface $media
     * @param File $in
     * @param File $out
     * @param string $format
     * @param array $settings
     *
     * @return void
     */
    public function resize(MediaInterface $media, File $in, File $out, $format, array $settings)
    {
        $event = new ResizeEvent($media, $in, $out, $format, $settings);
        $this->eventDispatcher->dispatch(ResizerEvents::PRE_RESIZE, $event);

        $this->delegate->resize($media, $event->getIn(), $out, $format, $settings);

        $this->eventDispatcher->dispatch(ResizerEvents::POST_RESIZE, $event);
    }

    /**
     * @param MediaInterface $media
     * @param array $settings
     *
     * @return \Imagine\Image\Box
     */
    public function getBox(MediaInterface $media, array $settings)
    {
        $box = $this->delegate->getBox($media, $settings);
        $event = new GetBoxEvent($media, $box, $settings);
        $this->eventDispatcher->dispatch(ResizerEvents::GET_BOX, $event);
        return $event->getBox();
    }
}
