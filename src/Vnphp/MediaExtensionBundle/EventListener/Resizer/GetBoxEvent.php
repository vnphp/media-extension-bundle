<?php


namespace Vnphp\MediaExtensionBundle\EventListener\Resizer;

use Imagine\Image\BoxInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Symfony\Component\EventDispatcher\Event;

class GetBoxEvent extends Event
{
    /**
     * @var MediaInterface
     */
    protected $media;

    /**
     * @var BoxInterface
     */
    protected $box;

    /**
     * @var array
     */
    protected $settings;

    /**
     * GetBoxEvent constructor.
     * @param MediaInterface $media
     * @param BoxInterface $box
     * @param array $settings
     */
    public function __construct(MediaInterface $media, BoxInterface $box, array $settings)
    {
        $this->media = $media;
        $this->box = $box;
        $this->settings = $settings;
    }

    /**
     * @return BoxInterface
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * @param BoxInterface $box
     */
    public function setBox(BoxInterface $box)
    {
        $this->box = $box;
    }

    /**
     * @return MediaInterface
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }
}
