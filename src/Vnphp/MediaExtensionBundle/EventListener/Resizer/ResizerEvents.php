<?php

namespace Vnphp\MediaExtensionBundle\EventListener\Resizer;

final class ResizerEvents
{
    const GET_BOX = 'resizer.get_box';

    const PRE_RESIZE = 'resizer.pre_resize';

    const POST_RESIZE = 'resizer_post_resize';
}
