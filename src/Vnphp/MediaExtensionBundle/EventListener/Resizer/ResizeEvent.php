<?php


namespace Vnphp\MediaExtensionBundle\EventListener\Resizer;

use Gaufrette\File;
use Sonata\MediaBundle\Model\MediaInterface;
use Symfony\Component\EventDispatcher\Event;

class ResizeEvent extends Event
{
    /**
     * @var MediaInterface
     */
    protected $media;

    /**
     * @var File
     */
    protected $in;

    /**
     * @var File
     */
    protected $out;

    /**
     * @var string
     */
    protected $format;

    /**
     * @var array
     */
    protected $settings;

    /**
     * ResizeEvent constructor.
     * @param MediaInterface $media
     * @param File $in
     * @param File $out
     * @param string $format
     * @param array $settings
     */
    public function __construct(MediaInterface $media, File $in, File $out, $format, array $settings)
    {
        $this->media = $media;
        $this->in = $in;
        $this->out = $out;
        $this->format = $format;
        $this->settings = $settings;
    }

    /**
     * @return MediaInterface
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @return File
     */
    public function getIn()
    {
        return $this->in;
    }

    /**
     * @return File
     */
    public function getOut()
    {
        return $this->out;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }
}
