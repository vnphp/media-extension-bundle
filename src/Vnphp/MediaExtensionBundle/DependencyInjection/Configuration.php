<?php

namespace Vnphp\MediaExtensionBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('vnphp_media_extension');

        $this->addResizerSection($rootNode);

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }

    private function addResizerSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->arrayNode('resizer')
                ->children()
                    ->arrayNode('event_aware')
                    ->children()
                        ->scalarNode('delegate')
                            ->defaultValue('sonata.media.resizer.simple')
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
