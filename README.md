# Vnphp Media Extension bundle


[![build status](https://gitlab.com/vnphp/media-extension-bundle/badges/master/build.svg)](https://gitlab.com/vnphp/media-extension-bundle/commits/master)
[![code quality](https://insight.sensiolabs.com/projects/f0b7a302-cbce-4f6c-8186-bbb5b9488d3f/big.png)](https://insight.sensiolabs.com/projects/f0b7a302-cbce-4f6c-8186-bbb5b9488d3f)


## Installation 

```
composer require vnphp/media-extension-bundle
```

You need to have `git` installed. 

## Usage

Add the bundle to your `AppKernel.php`:

```php
<?php 

$bundles = array(          
            new \Vnphp\MediaExtensionBundle\VnphpMediaExtensionBundle(),
        );

```
